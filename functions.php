<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', [] );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', 20 );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );

/**
 * Enqueue scripts and styles.
 */
function ebo2_scripts() {

    wp_enqueue_style('Bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css', false, 'all');
    #wp_enqueue_style('owlCss', get_stylesheet_directory_uri() . '/css/owl.carousel.min.css', false, 'all');
    #wp_enqueue_style('owlCss', get_stylesheet_directory_uri() . '/css/owl.theme.default.min.css', false, 'all');
    wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/css/responsive.css', false, 'all');
    #wp_enqueue_style('aos', get_stylesheet_directory_uri() . '/css/aos.css', false, 'all');
}
add_action( 'wp_enqueue_scripts', 'ebo2_scripts', 70 );

function enqueue_my_script() {
    wp_enqueue_script('BootstrapJS', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), false, true);
    #wp_enqueue_script('Owljs', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), false, true);
    #wp_enqueue_script('aosjs', get_stylesheet_directory_uri() . '/js/aos.js', array('jquery'), false, true);
    #wp_enqueue_script('pararoller', get_stylesheet_directory_uri() . '/js/jquery.paroller.min.js', array('jquery'), false, true);
    wp_enqueue_script('main', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'), false, true);
}

add_action('wp_enqueue_scripts', 'enqueue_my_script');

