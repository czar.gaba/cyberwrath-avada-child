'use strict';
var fs = require('fs');
const gulp = require('gulp'),
    sass = require('gulp-sass')(require('sass')),
    postcss = require('gulp-postcss'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    foreach = require('gulp-flatmap'),
    browserSync = require('browser-sync').create(),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    changed = require('gulp-changed'),
    merge = require('merge-stream'),
    header = require('gulp-header');
    var sftp = require('gulp-sftp');
    const plugins = [
        autoprefixer,
        cssnano({
            preset: ['default', {
                discardComments: {
                    removeAll: false
                }
            }]
        })
    ];



//GULP SFTP HERE                                                               
var devx = JSON.parse(fs.readFileSync('./development.json'));



const paths = {
    styles: {
        src: './style.scss',
        dest: './'
    },
    scripts: {
        src: [
            './assets/js/sources/*.js',
            './node_modules/jquery/dist/jquery.js',
            './node_modules/popper.js/dist/umd/popper.js',
            './node_modules/bootstrap/dist/js/bootstrap.js'
        ],
        dest: './assets/js'
    },
    site: {
        url: devx.url
    },
    
}

const pkg = require('./package.json');

const banner = [
    '/**',
    '* <%= pkg.name %> - <%= pkg.description %>',
    '* @version v<%= pkg.version %>',
    '* @link <%= pkg.repository %>',
    '* @license <%= pkg.license %>',
    '*/',
    ''
].join('\n');






function watch() {
  
   
  
    gulp.watch(['./sass/uncompiled/*.scss'], testcomp);
    gulp.watch(['./css/*.css'], sftpcss);
    gulp.watch(['./*.php'], sftpfn);


        }

function testcomp(){


    return gulp.src([
    './sass/uncompiled/*.scss'])
    .pipe(sass().on('error', sass.logError))
    //.pipe(concatFilenames('manifest.txt', concatFilenamesOptions))
    // .pipe(concat('all.css'))
    .pipe(postcss(plugins))
    .pipe(gulp.dest('./css/'));

}







function sftpcss(){
    return gulp.src('./css/*.css')
    .pipe(sftp({
        host: devx.host,
        user: devx.user,
        pass: devx.pass,  
        remotePath: devx.csspath
    }));
}

function sftpfn(){
    return gulp.src('./*.php')
    .pipe(sftp({
        host: devx.host,
        user: devx.user,
        pass: devx.pass, 
        remotePath: devx.rootpath
    }));
}

gulp.task('default', gulp.series( testcomp,   watch));
