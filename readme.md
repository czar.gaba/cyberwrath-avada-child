# Cyberwrath Avada Child
## Requirements
- <a href="https://code.visualstudio.com/download"> VSCODE </a>
- <a href="https://nodejs.org/en/download/"> Node JS </a>
- <a href="https://git-scm.com/downloads"> Git Bash </a>

## Steps 
1. Download  or fork the repository  
2. Install Gulp in Node/Bash (gulp, gulp-cli)
3. Add/Create File `development.json` and get custom code from me!

## NOTE

To check if modules are installed type in Terminal `gulp --version`

4. Run NPM -> `npm i` / `npm i gulp -g gulp-cli`
5. Go to `node_modules/gulp-sftp` and find file.pipe(stream);
6. Remove `file.pipe(stream);` and replace with this [code below]

```
if ( file.isStream() ) {
            file.contents.pipe( stream );
        } else if ( file.isBuffer() ) {
            stream.end( file.contents );
    }
```
Final Output
``` 
// file.pipe(stream); // start upload
    if ( file.isStream() ) {
            file.contents.pipe( stream );
        } else if ( file.isBuffer() ) {
            stream.end( file.contents );
    }
```
7. Run Gulp -> Type in Terminal "gulp".
